﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSA_hw_1.BLL.Models;
using BSA_hw_1.Models;
using BSA_hw_1.Models.ForQueries;

namespace BSA_hw_1
{
    public class LinqQueries
    {
        public static List<ProjectDTO> projects;
        public static List<TasksDTO> tasks;
        public static List<UserDTO> users;
        public static List<TeamDTO> teams;
        public static List<Project> projInfo = new List<Project>();
        public static async Task<List<Project>> GetAll()
        {
            projects = await ApiRequests.GetProjects();
            tasks = await ApiRequests.GetTasks();
            users = await ApiRequests.GetUsers();
            teams = await ApiRequests.GetTeams();

            var tasksAndPerformers = tasks.Join(users, t => t.PerformerId, u => u.Id, (t, u) => new Tasks(t, u));
            var teamsAndParticipants = teams.GroupJoin(users, t => t.Id, u => u.TeamId, (t, u) => new Team(t, u));
            var all = projects.GroupJoin(tasksAndPerformers, p => p.Id, tks => tks.ProjectId, (p, tks) => new { project = p, tasks = tks })
                .Join(users, p => p.project.AuthorId, u => u.Id, (p, u) => new { project = p.project, tasks = p.tasks, author = u })
                .Join(teamsAndParticipants, p => p.project.TeamId, tms => tms.Id, (p, tms) => new Project(p.project, p.tasks, p.author, tms));

            projInfo = all.ToList();
            return projInfo;
        }
        public static Dictionary<Project, int> GetQuantityOfUserTasks(int id)
        {
            ////as a task performer
            //var info = projInfo
            //    .SelectMany(pr => pr.Tasks)
            //    .Where(x => x.Performer.Id == id)
            //    .GroupBy(y => projInfo.First(x => x.Id == y.ProjectId))
            //    .ToDictionary(x => x.Key, x => x.Count());

            ////as a project author
            var info = projInfo
                .Where(x => x.Author.Id == id)
                .SelectMany(pr => pr.Tasks)
                .GroupBy(y => projInfo.First(x => x.Id == y.ProjectId))
                .ToDictionary(x => x.Key, x => x.Count());

            return info;
        }
        public static List<Tasks> GetUserTasks(int id)
        {
            var info = projInfo
                .SelectMany(pr => pr.Tasks)
                .Where(x => x.Performer.Id == id && x.Name.Length < 45)
                .ToList();

            return info;
        }
        public static List<(int id, string name)> GetUserFinishedTasks(int id)
        {
            var info = projInfo
                .SelectMany(p => p.Tasks)
                .Where(x => x.Performer.Id == id && x.FinishedAt <= DateTime.Now && x.FinishedAt >= new DateTime(2021, 1, 1))
                .Select(x => (id: x.Id, name: x.Name))
                .ToList();

            return info;
        }
        public static List<(int id, string name, List<UserDTO> users)> GetSortedUsersTeams()
        {
            var info = projInfo
                .GroupBy(p => p.Team)
                .Select(x => x.First())
                .Select(x => (x.Team.Id, x.Team.Name, x.Team.Participants
                .Where(p => p.BirthDay <= DateTime.Now.AddYears(-10))
                .OrderByDescending(p => p.RegisteredAt).ToList()))
                .ToList();

            return info;
        }
        public static List<IGrouping<UserDTO, Tasks>> GetSortedUsersWithTasks()
        {
            var info = projInfo
                .SelectMany(p => p.Tasks)
                .OrderByDescending(t => t.Name.Length)
                .GroupBy(t => t.Performer)
                .OrderBy(x => x.Key.FirstName)
                .ToList();

            return info;
        }
        public static UserTaskInfo GetUserTasksInfo(int id)
        {
            var info = projInfo
                .SelectMany(p => p.Tasks)
                .Where(x => x.Performer.Id == id)
                .GroupBy(x => x.Performer)
                .Where(x => x.Key.Id == id)
                .Select(x => new { 
                    user = x.Key,
                    lastProject = projInfo.Where(p => p.Team.Participants.Contains(x.Key)).OrderByDescending(x => x.CreatedAt).FirstOrDefault(),
                    tasks = x
                }).Select(x=> new UserTaskInfo() { 
                    User = x.user, 
                    LastProject = x.lastProject,
                    LastProjectTasksCount = x.lastProject?.Tasks.Count() ?? 0,
                    RejectedTasks = x.tasks.Where(x => x.FinishedAt == null | (int)x.State == 2).Count(),
                    TheLongestTask = x.tasks.OrderByDescending(x => (x.FinishedAt ?? DateTime.Now) - x.CreatedAt).FirstOrDefault()
                }).FirstOrDefault();

            return info;
        }
        public static List<ProjectsInfo> GetProjectsInfo()
        {
            var info = projInfo.Select(p => new ProjectsInfo
            {
                Project = p,
                LongestTaskByDescr = p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                ShortestTaskByName = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                UsersCount = p.Description.Length > 20 ^ p.Tasks.Count < 3 ? p.Team.Participants.Count : 0
            }).ToList();

            return info;
        }
    }
}
