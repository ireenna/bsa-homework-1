﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSA_hw_1.BLL.Models;

namespace BSA_hw_1.Models
{
    public class Tasks
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public UserDTO Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskStateDTO State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public Tasks(TasksDTO task, UserDTO u)
        {
            Id = task.Id;
            ProjectId = task.ProjectId;
            Performer = u;
            Name = task.Name;
            Description = task.Description;
            State = task.State;
            CreatedAt = task.CreatedAt;
            FinishedAt = task.FinishedAt;
        }
    }
}
