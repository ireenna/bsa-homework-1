﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSA_hw_1.BLL.Models;

namespace BSA_hw_1.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<UserDTO> Participants { get; set; }
        public Team(TeamDTO t, IEnumerable<UserDTO> u)
        {
            Id = t.Id;
            Name = t.Name;
            CreatedAt = t.CreatedAt;
            Participants = u.ToList();
        }
    }
}
