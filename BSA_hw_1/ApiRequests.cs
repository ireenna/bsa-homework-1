﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BSA_hw_1.BLL.Models;
using Newtonsoft.Json;

namespace BSA_hw_1
{
    public class ApiRequests
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly string localpath = $"https://bsa21.azurewebsites.net/api/";
        public static async Task<List<ProjectDTO>> GetProjects()
        {
            HttpResponseMessage response = await client.GetAsync(localpath + "Projects");
            string strResponse = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ProjectDTO>>(strResponse);
        }
        public static async Task<List<TasksDTO>> GetTasks()
        {
            HttpResponseMessage response = await client.GetAsync(localpath + "Tasks");
            string strResponse = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<TasksDTO>>(strResponse);
        }
        public static async Task<List<UserDTO>> GetUsers()
        {
            HttpResponseMessage response = await client.GetAsync(localpath + "Users");
            string strResponse = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<UserDTO>>(strResponse);
        }
        public static async Task<List<TeamDTO>> GetTeams()
        {
            HttpResponseMessage response = await client.GetAsync(localpath + "Teams");
            string strResponse = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<TeamDTO>>(strResponse);
        }
    }
}
