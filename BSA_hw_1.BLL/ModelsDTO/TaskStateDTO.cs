﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA_hw_1.BLL.Models
{
    public enum TaskStateDTO
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}
